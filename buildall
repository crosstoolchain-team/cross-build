#!/bin/sh

set -e

# script to run builds for each source. You should really run this in a chroot

PARALLEL=${PARALLEL:=2}
PKG_LIST=${PKG_LIST:="binutils gcc gcc-defaults"}
#Set of supported targets
TARGET_LIST=${TARGET_LIST:="armel armhf mips mipsel powerpc i386"}


LOCAL_REPO=/var/cache/cross-build-repo

mkdir -p $LOCAL_REPO

echo "deb [trusted=yes] file:///var/cache/cross-build-repo ./" > /etc/apt/sources.list.d/cross-build-repo.list
(cd $LOCAL_REPO && apt-ftparchive packages . > Packages)

echo -n > ../../failures

mkdir -p packages && cd packages

for arch in $TARGET_LIST; do
    export DEB_TARGET_GNU_TYPE=$(dpkg-architecture -a$arch -qDEB_HOST_GNU_TYPE -f 2>/dev/null)
    dpkg --add-architecture $arch

    for pkg in $PKG_LIST; do
	apt-get update
	(
	    set -e
	    export DEB_BUILD_OPTIONS=${DEB_BUILD_OPTIONS:=parallel=$PARALLEL}
	    cd cross-$pkg-$arch

	    mk-build-deps -i -t'apt-get -y'  debian/control

	    if ! dpkg -s cross-$pkg-$arch-build-deps >/dev/null 2>&1; then
		echo cross-$pkg-$arch >> ../../failures
		break
	    fi

	    dpkg-buildpackage -uc -us
	    if [ $? != 0 ]; then
		echo cross-$pkg-$arch >> ../../failures
		break
	    fi

	    find $(pwd)/.. -name '*.deb' -exec ln -fs {} $LOCAL_REPO \;
	    (cd $LOCAL_REPO && apt-ftparchive packages . > Packages)
	)

	apt-get purge -y cross-$pkg-$arch-build-deps
	apt-get -y autoremove

	leftover=$(dpkg -l | grep :$arch | awk '{ print $2 }')

	if [ "$leftover" ]; then
	    echo -n WARNING: Remaining packages from $arch after removing build-deps: $leftover >&2
	    apt-get purge -y $leftover
	fi
    done

    if ! dpkg --remove-architecture ${arch}; then
	echo "failed to tidy up"
	exit 1
    fi
done
